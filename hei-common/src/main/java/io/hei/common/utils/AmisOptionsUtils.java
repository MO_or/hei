

package io.hei.common.utils;

import java.io.Serializable;
import java.util.List;

/**
 * AMIS框架Options单选框工具类
 *
 * @author Mark MO_or
 */
public class AmisOptionsUtils implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 列表数据
	 */
	private List<?> options;

	public AmisOptionsUtils() {
	}

	public AmisOptionsUtils(List<?> options) {
		this.options = options;
	}

	public List<?> getOptions() {
		return options;
	}

	public void setOptions(List<?> options) {
		this.options = options;
	}
}
