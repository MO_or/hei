

package io.hei.common.utils;

import com.baomidou.mybatisplus.core.metadata.IPage;

import java.io.Serializable;
import java.util.List;

/**
 * AMIS框架CRUD增删改查分页工具类
 *
 * @author Mark MO_or
 */
public class AmisCRUDUtils implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 总记录数
	 */
	private long count;
	/**
	 * 列表数据
	 */
	private List<?> items;

	public AmisCRUDUtils() {
	}

	public AmisCRUDUtils(long count, List<?> items) {
		this.count = count;
		this.items = items;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public List<?> getItems() {
		return items;
	}

	public void setItems(List<?> items) {
		this.items = items;
	}
}
