

package io. hei.commons.dynamic.datasource.config;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * 多数据源
 *
 *
 * @since 1.0.0
 */
// 继承AbstractRoutingDataSource类，实现自己的数据源选择逻辑
public class DynamicDataSource extends AbstractRoutingDataSource {

    /**
     * 返回当前上下文环境的数据源key
     * 后续会根据这个key去找到对应的数据源属性
     */
    @Override
    protected Object determineCurrentLookupKey() {
        return DynamicContextHolder.peek();
    }

}
