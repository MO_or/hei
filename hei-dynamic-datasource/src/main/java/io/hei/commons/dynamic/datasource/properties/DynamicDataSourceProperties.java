

package io. hei.commons.dynamic.datasource.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 多数据源属性
 * @since 1.0.0
 */

/**
 * 通过@ConfigurationProperties指定读取yml的前缀关键字
 * 配合setDatasource()，即读取dynamic.datasource下的配置，将配置属性转化成bean
 * 容器执行顺序是，在bean被实例化后，会调用后置处理，递归的查找属性，通过反射注入值
 *
 * 由于该类只在DynamicDataSourceConfig类中使用，没有其它地方用到，所以没有使用@Component
 * 而是在DynamicDataSourceConfig类中用@EnableConfigurationProperties定义为bean
 */
@ConfigurationProperties(prefix = "dynamic")
public class DynamicDataSourceProperties {
    private Map<String, DataSourceProperties> datasource = new LinkedHashMap<>();

    public Map<String, DataSourceProperties> getDatasource() {
        return datasource;
    }

    public void setDatasource(Map<String, DataSourceProperties> datasource) {
        this.datasource = datasource;
    }
}
