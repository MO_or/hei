

package io. hei.commons.dynamic.datasource.annotation;

import java.lang.annotation.*;

/**
 * 多数据源注解
 *
 *
 * @since 1.0.0
 */
// 定义作用范围为（方法、接口、类、枚举、注解）
@Target({ElementType.METHOD, ElementType.TYPE})
// 保证运行时能被JVM或使用反射的代码使用
@Retention(RetentionPolicy.RUNTIME)
// 生成Javadoc时让使用了@DataSource这个注解的地方输出@DataSource这个注解或不同内容
// 仅当了解，可以不用加
@Documented
// 类继承中让子类继承父类@DataSource注解
@Inherited
public @interface DataSource {
    // @DataSource注解里传的参，这里主要传配置文件中不同数据源的标识，如@DataSource("slave1")
    String value() default "";
}
