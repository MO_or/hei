

package io. hei.commons.dynamic.datasource.config;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * 多数据源上下文操作支持
 */
public class DynamicContextHolder {
    /**
     * Lambda构造 本地线程变量
     * 用于避免多次创建数据库连接或者多线程使用同一个数据库连接
     * 减少数据库连接创建关闭对程序执行效率的影响与服务器压力
     *
     * 这里使用数组队列实现栈数据结构，实现函数局部状态所需的后进先出"LIFO"环境
     */
    private static final ThreadLocal<Deque<String>> CONTEXT_HOLDER = ThreadLocal.withInitial(ArrayDeque::new);

    /**
     * 获得当前线程数据源
     *
     * @return 数据源名称
     */
    public static String peek() {
        return CONTEXT_HOLDER.get().peek();
    }

    /**
     * 设置当前线程数据源
     *
     * @param dataSource 数据源名称
     */
    public static void push(String dataSource) {
        CONTEXT_HOLDER.get().push(dataSource);
    }

    /**
     * 清空当前线程数据源
     */
    public static void poll() {
        Deque<String> deque = CONTEXT_HOLDER.get();
        deque.poll();
        if (deque.isEmpty()) {
            CONTEXT_HOLDER.remove();
        }
    }

}