# 罗小黑的hei

## 声明

本项目整体框架主要基于[人人开源 / renren-security](https://gitee.com/renrenio/renren-security)，前端部分框架使用[百度开源 / amis](https://gitee.com/baidu/amis?_from=gitee_search)，基于以上框架进行整合、学习、二开，实践基本知识库管理，包含文档、文章管理，简易Netty聊天室demo，Redis可视化管理与监控，前端低代码框架等

PS：由于项目部署的服务器是学习服务器，配置很低，首次加载使用AMIS框架的页面（文档管理、文章管理、文库搜索）非常慢（40s以上）

## 项目说明
* 采用SpringBoot、MyBatis、Shiro框架
* 提供了代码生成器
* 支持MySQL、Oracle、SQL Server、PostgreSQL等主流数据库
* 支持多数据源切换
* 支持低代码前端框架

## 具有如下特点

* 灵活的权限控制，可控制到页面或按钮，满足绝大部分的权限需求
* 完善的部门管理及数据权限，通过注解实现数据权限的控制
* 完善的XSS防范及脚本过滤，彻底杜绝XSS攻击
* 支持分布式部署，session存储在redis中
* 友好的代码结构及注释，便于阅读及二次开发
* 引入quartz定时任务，可动态完成任务的添加、修改、删除、暂停、恢复及日志查看等功能
* 页面交互使用Vue2.x，极大的提高了开发效率
* 引入swagger文档支持，方便编写API接口文档
* 支持多数据源切换
* 支持低代码前端框架


## 数据权限设计思想

* 管理员管理、角色管理、部门管理，可操作本部门及子部门数据
* 菜单管理、定时任务、参数管理、字典管理、系统日志，没有数据权限
* 业务功能，按照用户数据权限，查询、操作数据【没有本部门数据权限，也能查询本人数据】

## 项目结构

```
hei-security
├─hei-common     公共模块
│ 
├─hei-admin      管理后台
│    ├─db  数据库SQL脚本
│    │ 
│    ├─modules  模块
│    │    ├─job 定时任务
│    │    ├─oss 文件存储
│    │    └─sys 系统管理(核心)
│    │    └─kbms 知识库
│    │    └─netty 聊天室
│    │ 
│    └─resources 
│        ├─mapper   MyBatis文件
│        ├─statics  静态资源
│        ├─template 系统页面
│        │    ├─modules      模块页面
│        │    ├─index.html   AdminLTE主题风格（默认主题）
│        │    └─index1.html  Layui主题风格
│        └─application.yml   全局配置文件
│   
│      
├─hei-api        API服务
│
│ 
├─hei-dynamic-datasource  多数据源
│
│ 
├─hei-generator  代码生成器
│        └─resources 
│           ├─mapper   MyBatis文件
│           ├─template 代码生成器模板（可增加或修改相应模板）
│           ├─application.yml    全局配置文件
│           └─generator.properties   代码生成器，配置文件
│
```

## 技术选型：

* 核心框架：Spring Boot 2.4.2
* 安全框架：Apache Shiro 1.4
* 视图框架：Spring MVC 5.0
* 持久层框架：MyBatis 3.5
* 定时器：Quartz 2.3
* 数据库连接池：Druid 1.1
* 日志管理：SLF4J 1.7、Log4j
* 页面交互：Vue2.x
* 低代码前端框架：AMIS

## 软件需求

* JDK1.8
* MySQL5.5+
* Maven3.0+

## 部署方式

Docker部署

## 项目演示

演示地址：[http://www.mo-or.cc](http://www.mo-or.cc)
账号密码：admin/admin