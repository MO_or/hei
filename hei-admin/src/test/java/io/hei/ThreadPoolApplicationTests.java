package io.hei;

import io.hei.modules.thread.Input;
import io.hei.modules.thread.Input2OutputService;
import io.hei.modules.thread.Output;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ThreadPoolApplicationTests {

    @Autowired
    private Input2OutputService input2OutputService;

    public ThreadPoolApplicationTests(){

    }

    @Test
    public void contextLoads() {
    }

    @Test
    public void multiProcess() {
        log.info("Multi process start");
        List<Input> inputList = Arrays.asList(new Input(1), new Input(2), new Input(3));
        System.out.println(input2OutputService.multiProcess(inputList));
        log.info("Multi process end");
    }

    @Test
    public void asyncProcess() throws InterruptedException, ExecutionException {
        log.info("Async process start");
        Future<Output> future = input2OutputService.asyncProcess(new Input(1));
        while (true) {
            if (future.isDone()) {
                System.out.println(future.get());
                log.info("Async process end");
                break;
            }
            log.info("Continue doing something else.");
            Thread.sleep(100);
        }
    }
}
