package io.hei;

import io.hei.common.utils.RedisUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootTest
public class StreamTest {

    @Test
    public void contextLoads() {
        Long[] longArr = new Long[3];
        longArr[0] = 60L;
        longArr[1] = 20L;
        longArr[2] = 50L;
        Arrays.stream(longArr).sorted().forEach(System.out::println);
    }

}
