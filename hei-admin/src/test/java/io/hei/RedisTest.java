package io.hei;

import io.hei.common.utils.RedisUtils;
import io.hei.modules.sys.entity.SysUserEntity;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTest {
    @Autowired
    private RedisUtils redisUtils;

    @Test
    public void contextLoads() {
//        SysUserEntity user = new SysUserEntity();
//        user.setEmail("123456@qq.com");
//        redisUtils.set("user", user);
        HashMap test = new HashMap();
        test.put("1","1");
        // redisUtils.set("testStringKey1", "testStringKey1", RedisUtils.NOT_EXPIRE);
        redisUtils.set("testStringKey2", test, RedisUtils.NOT_EXPIRE);

//        System.out.println("RedisTest1:" + ToStringBuilder.reflectionToString(redisUtils.get("user", SysUserEntity.class)));
        System.out.println("RedisTest:" + redisUtils.get("testStringKey2"));
    }

}
