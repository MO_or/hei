$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'kbms/kbmsfilemain/list',
        datatype: "json",
        colModel: [			
			{ label: 'fileId', name: 'fileId', index: 'file_id', width: 50, key: true },
			{ label: '用户编号', name: 'userId', index: 'user_id', width: 80 }, 			
			{ label: '存储路径', name: 'path', index: 'path', width: 80 }, 			
			{ label: '创建时间', name: 'createTime', index: 'create_time', width: 80 }, 			
			{ label: '分类编号', name: 'classifyId', index: 'classify_id', width: 80 }, 			
			{ label: '源文件名称', name: 'orignName', index: 'orign_name', width: 80 }, 			
			{ label: '文件名称', name: 'fileName', index: 'file_name', width: 80 }, 			
			{ label: '文件类型', name: 'fileType', index: 'file_type', width: 80 }, 			
			{ label: '文件大小', name: 'fileSize', index: 'file_size', width: 80 }, 			
			{ label: '评论数', name: 'commentNum', index: 'comment_num', width: 80 }, 			
			{ label: '下载数', name: 'downloadNum', index: 'download_num', width: 80 }, 			
			{ label: '点赞数', name: 'likeNum', index: 'like_num', width: 80 }, 			
			{ label: '是否删除：0、否，1、是，默认0', name: 'deleteStatus', index: 'delete_status', width: 80 }			
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		kbmsFileMain: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.kbmsFileMain = {};
		},
		update: function (event) {
			var fileId = getSelectedRow();
			if(fileId == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(fileId)
		},
		saveOrUpdate: function (event) {
		    $('#btnSaveOrUpdate').button('loading').delay(1000).queue(function() {
                var url = vm.kbmsFileMain.fileId == null ? "kbms/kbmsfilemain/save" : "kbms/kbmsfilemain/update";
                $.ajax({
                    type: "POST",
                    url: baseURL + url,
                    contentType: "application/json",
                    data: JSON.stringify(vm.kbmsFileMain),
                    success: function(r){
                        if(r.code === 0){
                             layer.msg("操作成功", {icon: 1});
                             vm.reload();
                             $('#btnSaveOrUpdate').button('reset');
                             $('#btnSaveOrUpdate').dequeue();
                        }else{
                            layer.alert(r.msg);
                            $('#btnSaveOrUpdate').button('reset');
                            $('#btnSaveOrUpdate').dequeue();
                        }
                    }
                });
			});
		},
		del: function (event) {
			var fileIds = getSelectedRows();
			if(fileIds == null){
				return ;
			}
			var lock = false;
            layer.confirm('确定要删除选中的记录？', {
                btn: ['确定','取消'] //按钮
            }, function(){
               if(!lock) {
                    lock = true;
		            $.ajax({
                        type: "POST",
                        url: baseURL + "kbms/kbmsfilemain/delete",
                        contentType: "application/json",
                        data: JSON.stringify(fileIds),
                        success: function(r){
                            if(r.code == 0){
                                layer.msg("操作成功", {icon: 1});
                                $("#jqGrid").trigger("reloadGrid");
                            }else{
                                layer.alert(r.msg);
                            }
                        }
				    });
			    }
             }, function(){
             });
		},
		getInfo: function(fileId){
			$.get(baseURL + "kbms/kbmsfilemain/info/"+fileId, function(r){
                vm.kbmsFileMain = r.kbmsFileMain;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});