package io.hei.common.annotation;

import java.lang.annotation.*;

/**
 * 用户id
 * 在controller接口入参中加入这个注解，直接把用户id作为入参传入方法
 * @author Admin
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface UserId {
}
