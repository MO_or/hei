

package io.hei.common.aspect;

import io.hei.common.exception.RRException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 切面处理类
 *
 *
 */
@Aspect
@Component
public class TestAspect {
    private Logger logger = LoggerFactory.getLogger(getClass());
    /**
     * 是否开启切面类  true开启   false关闭
     */
    @Value("${hei.test.open: false}")
    private boolean open;

    // @Around("execution(* io.hei.modules.kbms.controller..*.*(..))")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        Object result = null;
        if(open){
            try{
                result = point.proceed();
            }catch (Exception e){
                logger.error("redis error", e);
                throw new RRException("Redis服务异常");
            }
        }
        return result;
    }
}
