

package io.hei.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * WebMvc配置
 *
 *
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Value("${files.imagePrefix}")
    private String imagePrefix;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/image/**").addResourceLocations(imagePrefix);
        registry.addResourceHandler("/statics/**").addResourceLocations("classpath:/statics/");
    }

}