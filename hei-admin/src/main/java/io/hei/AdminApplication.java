

package io.hei;

import io.hei.modules.netty.WebSocketServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@SpringBootApplication
public class AdminApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext run = SpringApplication.run(AdminApplication.class, args);
		WebSocketServer webSocketServer = run.getBean("WebSocketServer", WebSocketServer.class);
		ExecutorService service = Executors.newCachedThreadPool();
		service.execute(webSocketServer);
		service.shutdown();
	}

}
