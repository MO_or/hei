package io.hei.modules.thread;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Output {
    boolean success;
    String s;
}
