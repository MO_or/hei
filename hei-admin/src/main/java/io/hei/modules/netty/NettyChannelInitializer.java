package io.hei.modules.netty;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.stream.ChunkedWriteHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@ChannelHandler.Sharable
public class NettyChannelInitializer extends ChannelInitializer<SocketChannel> {

    @Value("${netty.websocket.max-frame-size}")
    private int maxContentLength;

    @Value("${netty.websocket.path}")
    private String websocketPath;

    @Autowired
    private ChatChannelHandler chatChannelHandler;

    @Override
    protected void initChannel(SocketChannel channel) throws Exception {
        //获取管道
        ChannelPipeline pipeline = channel.pipeline();
        //添加http编解码器
        pipeline.addLast(new HttpServerCodec());
        //对写大数据流进行支持
        pipeline.addLast(new ChunkedWriteHandler());
        //对httpMessage进行聚合处理
        pipeline.addLast(new HttpObjectAggregator(maxContentLength));
        //处理握手动作
        pipeline.addLast(new WebSocketServerProtocolHandler(websocketPath));

        pipeline.addLast(chatChannelHandler);
    }
}

