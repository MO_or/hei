package io.hei.modules.kbms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.hei.common.utils.AmisCRUDUtils;
import io.hei.common.utils.PageUtils;
import io.hei.common.utils.Query;
import io.hei.modules.kbms.dao.KbmsFileCommentDao;
import io.hei.modules.kbms.entity.KbmsFileCommentEntity;
import io.hei.modules.kbms.service.KbmsFileCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("kbmsFileCommentService")
public class KbmsFileCommentServiceImpl extends ServiceImpl<KbmsFileCommentDao, KbmsFileCommentEntity> implements KbmsFileCommentService {

    @Autowired
    private KbmsFileCommentDao kbmsFileCommentDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<KbmsFileCommentEntity> page = this.page(
                new Query<KbmsFileCommentEntity>().getPage(params),
                new QueryWrapper<KbmsFileCommentEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public AmisCRUDUtils selectByFileId(Long fileId, int page, int perPage) {

        // 组装分页参数
        PageHelper.startPage(page, perPage);
        List<Map<String, ?>> maps = kbmsFileCommentDao.selectByFileId(fileId);
        PageInfo pageInfo = new PageInfo<>(maps);

        return new AmisCRUDUtils(pageInfo.getTotal(), pageInfo.getList());
    }

}
