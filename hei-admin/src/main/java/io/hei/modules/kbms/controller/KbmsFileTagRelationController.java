package io.hei.modules.kbms.controller;

import java.util.Arrays;
import java.util.Map;

import io.hei.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.hei.modules.kbms.entity.KbmsFileTagRelationEntity;
import io.hei.modules.kbms.service.KbmsFileTagRelationService;
import io.hei.common.utils.PageUtils;
import io.hei.common.utils.R;



/**
 * 文档标签关系表
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-31 15:28:24
 */
@RestController
@RequestMapping("sys/kbmsfiletagrelation")
public class KbmsFileTagRelationController {
    @Autowired
    private KbmsFileTagRelationService kbmsFileTagRelationService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:kbmsfiletagrelation:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = kbmsFileTagRelationService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:kbmsfiletagrelation:info")
    public R info(@PathVariable("id") Long id){
        KbmsFileTagRelationEntity kbmsFileTagRelation = kbmsFileTagRelationService.getById(id);

        return R.ok().put("kbmsFileTagRelation", kbmsFileTagRelation);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:kbmsfiletagrelation:save")
    public R save(@RequestBody KbmsFileTagRelationEntity kbmsFileTagRelation){
        kbmsFileTagRelationService.save(kbmsFileTagRelation);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:kbmsfiletagrelation:update")
    public R update(@RequestBody KbmsFileTagRelationEntity kbmsFileTagRelation){
        ValidatorUtils.validateEntity(kbmsFileTagRelation);
        kbmsFileTagRelationService.updateById(kbmsFileTagRelation);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:kbmsfiletagrelation:delete")
    public R delete(@RequestBody Long[] ids){
        kbmsFileTagRelationService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
