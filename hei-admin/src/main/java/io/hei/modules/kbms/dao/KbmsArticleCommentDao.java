package io.hei.modules.kbms.dao;

import io.hei.modules.kbms.entity.KbmsArticleCommentEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 文章评论表
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-30 09:06:12
 */
@Mapper
@Repository
public interface KbmsArticleCommentDao extends BaseMapper<KbmsArticleCommentEntity> {

    List<Map<String, ?>> selectByArticleId(Long articleId);
}
