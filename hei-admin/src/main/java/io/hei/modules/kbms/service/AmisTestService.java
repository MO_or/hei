package io.hei.modules.kbms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.hei.common.utils.PageUtils2;
import io.hei.modules.kbms.entity.AmisTestEntity;

import java.util.Map;

/**
 * 
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-07-22 15:31:34
 */
public interface AmisTestService extends IService<AmisTestEntity> {

    PageUtils2 queryPage(Map<String, Object> params);
}

