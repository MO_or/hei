package io.hei.modules.kbms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.hei.common.utils.AmisCRUDUtils;
import io.hei.common.utils.PageUtils;
import io.hei.common.utils.Query;
import io.hei.modules.kbms.dao.KbmsArticleCommentDao;
import io.hei.modules.kbms.entity.KbmsArticleCommentEntity;
import io.hei.modules.kbms.service.KbmsArticleCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("kbmsArticleCommentService")
public class KbmsArticleCommentServiceImpl extends ServiceImpl<KbmsArticleCommentDao, KbmsArticleCommentEntity> implements KbmsArticleCommentService {

    @Autowired
    private KbmsArticleCommentDao kbmsArticleCommentDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<KbmsArticleCommentEntity> page = this.page(
                new Query<KbmsArticleCommentEntity>().getPage(params),
                new QueryWrapper<KbmsArticleCommentEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public AmisCRUDUtils selectByArticleId(Long articleId, int page, int perPage) {

        // 组装分页参数
        PageHelper.startPage(page, perPage);
        List<Map<String, ?>> maps = kbmsArticleCommentDao.selectByArticleId(articleId);
        PageInfo pageInfo = new PageInfo<>(maps);

        return new AmisCRUDUtils(pageInfo.getTotal(), pageInfo.getList());
    }
}
