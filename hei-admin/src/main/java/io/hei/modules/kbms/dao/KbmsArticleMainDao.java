package io.hei.modules.kbms.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.hei.modules.kbms.entity.KbmsArticleMainDTO;
import io.hei.modules.kbms.entity.KbmsArticleMainEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 文章主档表
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-30 09:05:50
 */
@Mapper
@Repository
public interface KbmsArticleMainDao extends BaseMapper<KbmsArticleMainEntity> {

    List<Map<String, ?>> selectByParams(KbmsArticleMainDTO kbmsArticleMainDTO);

    List<Map<String, ?>> selectFullText(KbmsArticleMainDTO kbmsArticleMainDTO);
}
