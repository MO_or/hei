package io.hei.modules.kbms.dao;

import io.hei.modules.kbms.entity.KbmsArticleContentEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 文章内容表
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-30 09:05:53
 */
@Mapper
public interface KbmsArticleContentDao extends BaseMapper<KbmsArticleContentEntity> {
	
}
