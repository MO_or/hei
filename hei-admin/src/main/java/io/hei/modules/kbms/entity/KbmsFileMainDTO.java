package io.hei.modules.kbms.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 文件主档表-视图层
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-06-29 14:24:48
 */
@Data
@TableName("kbms_file_main")
public class KbmsFileMainDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 文档编号
	 */
	@TableId
	private Long id;
	/**
	 * 文件编号
	 */
	private String fileId;
	/**
	 * 用户编号
	 */
	private Long userId;
	/**
	 * 存储路径
	 */
	private String path;
	/**
	 * 创建时间
	 */
	private Long createTime;
	/**
	 * 分类编号
	 */
	private Long classifyId;
	/**
	 * 源文件名称
	 */
	private String originName;
	/**
	 * 文件名称
	 */
	private String fileName;
	/**
	 * 文件类型
	 */
	private String fileType;
	/**
	 * 文件大小
	 */
	private String fileSize;
	/**
	 * 评论数
	 */
	private Integer commentNum;
	/**
	 * 下载数
	 */
	private Integer downloadNum;
	/**
	 * 点赞数
	 */
	private Integer likeNum;
	/**
	 * 是否删除：0、否，1、是，默认0
	 */
	private Integer deleteStatus;
	/**
	 * 用户姓名
	 */
	private String userName;
	/**
	 * 标签名称
	 */
	private String tagName;
	/**
	 * 标签编号
	 */
	private String tagId;
	/**
	 * 文档编号按逗号分隔用于in查询
	 */
	private String ids;
	/**
	 * 全文检索
	 */
	private String fullText;
	/**
	 * 分类编号查询参数
	 */
	private String classifyIds;
}
