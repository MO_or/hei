package io.hei.modules.kbms.controller;

import io.hei.common.utils.PageUtils;
import io.hei.common.utils.R;
import io.hei.common.validator.ValidatorUtils;
import io.hei.modules.kbms.entity.KbmsArticleContentEntity;
import io.hei.modules.kbms.service.KbmsArticleContentService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * 文章内容表
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-30 09:05:53
 */
@RestController
@RequestMapping("kbms/kbmsarticlecontent")
public class KbmsArticleContentController {
    @Autowired
    private KbmsArticleContentService kbmsArticleContentService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:kbmsarticlecontent:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = kbmsArticleContentService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{articleId}")
    @RequiresPermissions("sys:kbmsarticlecontent:info")
    public R info(@PathVariable("articleId") Long articleId){
        KbmsArticleContentEntity kbmsArticleContent = kbmsArticleContentService.getById(articleId);

        return R.ok().put("kbmsArticleContent", kbmsArticleContent);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:kbmsarticlecontent:save")
    public R save(@RequestBody KbmsArticleContentEntity kbmsArticleContent){
        kbmsArticleContentService.save(kbmsArticleContent);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:kbmsarticlecontent:update")
    public R update(@RequestBody KbmsArticleContentEntity kbmsArticleContent){
        ValidatorUtils.validateEntity(kbmsArticleContent);
        kbmsArticleContentService.updateById(kbmsArticleContent);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:kbmsarticlecontent:delete")
    public R delete(@RequestBody Long[] articleIds){
        kbmsArticleContentService.removeByIds(Arrays.asList(articleIds));

        return R.ok();
    }

}
