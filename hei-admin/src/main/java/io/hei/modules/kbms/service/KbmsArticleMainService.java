package io.hei.modules.kbms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.hei.common.utils.AmisCRUDUtils;
import io.hei.common.utils.PageUtils;
import io.hei.modules.kbms.entity.KbmsArticleMainDTO;
import io.hei.modules.kbms.entity.KbmsArticleMainEntity;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 文章主档表
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-30 09:05:50
 */
public interface KbmsArticleMainService extends IService<KbmsArticleMainEntity> {

    AmisCRUDUtils selectByParams(KbmsArticleMainDTO kbmsArticleMainDTO, int page, int perPage);

    AmisCRUDUtils selectFullText(KbmsArticleMainDTO kbmsArticleMainDTO, int page, int perPage);

    PageUtils queryPage(Map<String, Object> params);

    void export(Map<String, Object> params, HttpServletResponse response);
}

