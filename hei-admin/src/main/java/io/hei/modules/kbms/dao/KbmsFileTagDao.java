package io.hei.modules.kbms.dao;

import io.hei.modules.kbms.entity.KbmsFileTagEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 文件标签表
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-04 14:25:22
 */
@Mapper
@Repository
public interface KbmsFileTagDao extends BaseMapper<KbmsFileTagEntity> {

    List<Map<String, ?>> selectAllTags();

    List<String> selectFileIdByTagId(String tagId);
}
