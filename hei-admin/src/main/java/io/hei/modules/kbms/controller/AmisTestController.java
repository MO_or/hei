package io.hei.modules.kbms.controller;

import java.util.Arrays;
import java.util.Map;

import io.hei.common.utils.PageUtils2;
import io.hei.common.validator.ValidatorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.hei.modules.kbms.entity.AmisTestEntity;
import io.hei.modules.kbms.service.AmisTestService;
import io.hei.common.utils.R;



/**
 * 
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-07-22 15:31:34
 */
@RestController
@RequestMapping("kbms/amistest")
public class AmisTestController {
    @Autowired
    private AmisTestService amisTestService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils2 data = amisTestService.queryPage(params);

        return R.ok().put("data", data);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Integer id){
        AmisTestEntity amisTest = amisTestService.getById(id);

        return R.ok().put("amisTest", amisTest);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody AmisTestEntity amisTest){
        amisTestService.save(amisTest);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody AmisTestEntity amisTest){
        ValidatorUtils.validateEntity(amisTest);
        amisTestService.updateById(amisTest);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete/{id}")
    public R delete(@PathVariable("id") Integer id){
        amisTestService.removeById(id);

        return R.ok();
    }



}
