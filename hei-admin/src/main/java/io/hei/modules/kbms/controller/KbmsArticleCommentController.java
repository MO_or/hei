package io.hei.modules.kbms.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import io.hei.common.utils.AmisCRUDUtils;
import io.hei.common.validator.ValidatorUtils;
import io.hei.modules.kbms.entity.KbmsFileCommentEntity;
import io.hei.modules.sys.controller.AbstractController;
import io.hei.modules.sys.entity.SysUserEntity;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.hei.modules.kbms.entity.KbmsArticleCommentEntity;
import io.hei.modules.kbms.service.KbmsArticleCommentService;
import io.hei.common.utils.PageUtils;
import io.hei.common.utils.R;



/**
 * 文章评论表
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-30 09:06:12
 */
@RestController
@RequestMapping("kbms/kbmsarticlecomment")
public class KbmsArticleCommentController extends AbstractController {
    @Autowired
    private KbmsArticleCommentService kbmsArticleCommentService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        /**
         * 接收分页参数
         */
        int page = Integer.parseInt((String) params.get("page"));
        int perPage = Integer.parseInt((String) params.get("perPage"));
        Long id = Long.parseLong((String) params.get("id"));

        AmisCRUDUtils data = kbmsArticleCommentService.selectByArticleId(
                id, page, perPage);

        return R.ok().put("data", data);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{commentId}")
    @RequiresPermissions("sys:kbmsarticlecomment:info")
    public R info(@PathVariable("commentId") Long commentId){
        KbmsArticleCommentEntity kbmsArticleComment = kbmsArticleCommentService.getById(commentId);

        return R.ok().put("kbmsArticleComment", kbmsArticleComment);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody Map<String, Object> params){
        final SysUserEntity user = getUser();
        KbmsArticleCommentEntity kbmsArticleCommentEntity = new KbmsArticleCommentEntity();
        kbmsArticleCommentEntity.setCommentUserId(user.getUserId());
        kbmsArticleCommentEntity.setUserId(((Integer) params.get("user_id")).longValue());
        kbmsArticleCommentEntity.setCommentTime(new Date());
        kbmsArticleCommentEntity.setContent((String) params.get("new_comment_content"));
        kbmsArticleCommentEntity.setArticleId(((Integer) params.get("article_id")).longValue());

        kbmsArticleCommentService.save(kbmsArticleCommentEntity);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:kbmsarticlecomment:update")
    public R update(@RequestBody KbmsArticleCommentEntity kbmsArticleComment){
        ValidatorUtils.validateEntity(kbmsArticleComment);
        kbmsArticleCommentService.updateById(kbmsArticleComment);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:kbmsarticlecomment:delete")
    public R delete(@RequestBody Long[] commentIds){
        kbmsArticleCommentService.removeByIds(Arrays.asList(commentIds));

        return R.ok();
    }

}
