package io.hei.modules.kbms.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 文件评论表
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-27 10:50:07
 */
@Data
@TableName("kbms_file_comment")
public class KbmsFileCommentEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 评论编号
	 */
	@TableId
	private Long commentId;
	/**
	 * 文件编号
	 */
	private Long fileId;
	/**
	 * 评论用户编号
	 */
	private Long fileUserId;
	/**
	 * 目标用户编号
	 */
	private Long userId;
	/**
	 * 评论内容
	 */
	private String content;
	/**
	 * 评论时间
	 */
	private Date commentTime;

}
