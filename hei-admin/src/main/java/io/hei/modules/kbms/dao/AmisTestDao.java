package io.hei.modules.kbms.dao;

import io.hei.modules.kbms.entity.AmisTestEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-07-22 15:31:34
 */
@Mapper
public interface AmisTestDao extends BaseMapper<AmisTestEntity> {
	
}
