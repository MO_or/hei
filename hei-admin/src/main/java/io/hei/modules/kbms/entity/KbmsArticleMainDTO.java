package io.hei.modules.kbms.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 文章主档表
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-30 09:05:50
 */
@Data
@TableName("kbms_article_main")
public class KbmsArticleMainDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 文章编号
	 */
	@TableId
	private Long articleId;
	/**
	 * 作者编号
	 */
	private Long userId;
	/**
	 * 发表日期
	 */
	private Date createTime;
	/**
	 * 修改日期
	 */
	private Date alterTime;
	/**
	 * 分类编号
	 */
	private Long classifyId;
	/**
	 * 文章标题
	 */
	private String title;
	/**
	 * 点赞次数
	 */
	private Integer likeNum;
	/**
	 * 评论次数
	 */
	private Integer commentNum;
	/**
	 * 浏览次数
	 */
	private Integer browseNum;
	/**
	 * 是否公开：0、否，1、是，默认1
	 */
	private Integer publicStatus;
	/**
	 * 审核是否通过：0、否，1、是
	 */
	private Integer auditStatus;
	/**
	 * 审核时间
	 */
	private Date auditTime;
	/**
	 * 是否删除：0、否，1、是，默认0
	 */
	private Integer deleteStatus;
	/**
	 * 用户姓名
	 */
	private String userName;
	/**
	 * 标签名称
	 */
	private String tagName;
	/**
	 * 标签编号
	 */
	private String tagId;
	/**
	 * 全文检索
	 */
	private String fullText;
	/**
	 * 分类编号查询参数
	 */
	private String classifyIds;
}
