package io.hei.modules.kbms.controller;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.hei.common.utils.AmisCRUDUtils;
import io.hei.common.utils.R;
import io.hei.common.validator.ValidatorUtils;
import io.hei.modules.kbms.entity.KbmsFileMainDTO;
import io.hei.modules.kbms.entity.KbmsFileMainEntity;
import io.hei.modules.kbms.entity.KbmsFileTagRelationEntity;
import io.hei.modules.kbms.service.KbmsFileMainService;
import io.hei.modules.kbms.service.KbmsFileTagRelationService;
import io.hei.modules.sys.controller.AbstractController;
import io.hei.modules.sys.entity.SysUserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.*;


/**
 * 文件主档表
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-06-29 14:24:48
 */
@RestController
@RequestMapping("kbms/kbmsfilemain")
public class KbmsFileMainController extends AbstractController {
    @Autowired
    private KbmsFileMainService kbmsFileMainService;

    @Autowired
    private KbmsFileTagRelationService kbmsFileTagRelationService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {

        KbmsFileMainDTO kbmsFileMainDTO = new KbmsFileMainDTO();
        kbmsFileMainDTO.setFileName((String) params.get("file_name"));
        kbmsFileMainDTO.setTagId((String) params.get("tag_id"));

        /**
         * 接收侧边菜单栏联动请求参数
         */
        if(!"".equals(params.get("classifyId")) && params.get("classifyId")!=null){
            kbmsFileMainDTO.setClassifyIds((String) params.get("classifyId"));
        }

        /**
         * 接收分页参数
         */
        int page = Integer.parseInt((String) params.get("page"));
        int perPage = Integer.parseInt((String) params.get("perPage"));

        AmisCRUDUtils data = kbmsFileMainService.selectByParams(
                kbmsFileMainDTO, page, perPage);

        return R.ok().put("data", data);
    }

    /**
     * 全文搜索
     */
    @RequestMapping("/listByFullText")
    public R listByFullText(@RequestParam Map<String, Object> params) {

        final String full_text = (String) params.get("full_text");
        KbmsFileMainDTO kbmsFileMainDTO = new KbmsFileMainDTO();
        kbmsFileMainDTO.setFullText(full_text);

        /**
         * 接收分页参数
         */
        int page = Integer.parseInt((String) params.get("page"));
        int perPage = Integer.parseInt((String) params.get("perPage"));

        AmisCRUDUtils data = kbmsFileMainService.selectFullText(
                kbmsFileMainDTO, page, perPage);

        return R.ok().put("data", data);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id) {
        KbmsFileMainEntity kbmsFileMain = kbmsFileMainService.getById(id);

        return R.ok().put("kbmsFileMain", kbmsFileMain);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody Map<String, Object> params) {

        /**
         * 保存文档主表
         */
        final SysUserEntity user = getUser();
        KbmsFileMainEntity kbmsFileMain = new KbmsFileMainEntity();
        kbmsFileMain.setUser_id(user.getUserId());
        kbmsFileMain.setCreate_time(new Date());
        kbmsFileMain.setFile_id((String) params.get("file_id"));
        kbmsFileMain.setFile_name((String) params.get("file_name"));
        if(!"".equals(params.get("classify_id")) && params.get("classify_id")!=null){
            kbmsFileMain.setClassify_id(((Integer) params.get("classify_id")).longValue());
        }
        kbmsFileMainService.save(kbmsFileMain);

        /**
         * 保存文档与标签关系表
         */
        KbmsFileMainEntity newKbmsFileMain = kbmsFileMainService.
                getOne(new QueryWrapper<KbmsFileMainEntity>().
                        eq("file_id", (String) params.get("file_id")));
        String[] tag_ids=((String) params.get("tag_ids")).split("[\\,]");
        Collection<KbmsFileTagRelationEntity> relationList= new ArrayList<>();
        Arrays.asList(tag_ids).forEach(tag_id -> {
            KbmsFileTagRelationEntity kbmsFileTagRelationEntity = new KbmsFileTagRelationEntity();
            kbmsFileTagRelationEntity.setFileId(newKbmsFileMain.getId());
            kbmsFileTagRelationEntity.setTagId(Long.parseLong(tag_id));
            relationList.add(kbmsFileTagRelationEntity);
        });
        kbmsFileTagRelationService.saveBatch(relationList);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody KbmsFileMainDTO kbmsFileMain) throws ParseException {
        ValidatorUtils.validateEntity(kbmsFileMain);
        final String format = DateUtil.format(
                DateUtil.date(kbmsFileMain.getCreateTime() * 1000)
                , "yyyy-MM-dd HH:mm:ss");
        System.out.println(format);
        // kbmsFileMainService.updateById(kbmsFileMain);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete/{id}")
    public R delete(@PathVariable("id") Long id) {
        kbmsFileMainService.deleteByPrimaryKey(id);

        return R.ok();
    }

}
