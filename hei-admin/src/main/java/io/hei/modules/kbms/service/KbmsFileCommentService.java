package io.hei.modules.kbms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.hei.common.utils.AmisCRUDUtils;
import io.hei.common.utils.PageUtils;
import io.hei.modules.kbms.entity.KbmsFileCommentEntity;

import java.util.Map;

/**
 * 文件评论表
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-27 10:50:07
 */
public interface KbmsFileCommentService extends IService<KbmsFileCommentEntity> {

    PageUtils queryPage(Map<String, Object> params);

    AmisCRUDUtils selectByFileId(Long fileId, int page, int perPage);
}

