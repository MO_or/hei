package io.hei.modules.kbms.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.hei.modules.kbms.entity.KbmsFileCommentEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 文件评论表
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-27 10:50:07
 */
@Mapper
@Repository
public interface KbmsFileCommentDao extends BaseMapper<KbmsFileCommentEntity> {

    List<Map<String, ?>> selectByFileId(Long fileId);
}
