package io.hei.modules.kbms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.hei.common.utils.AmisCRUDUtils;
import io.hei.modules.kbms.entity.KbmsFileMainEntity;
import io.hei.modules.kbms.entity.KbmsFileMainDTO;

/**
 * 文件主档表
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-06-29 14:24:48
 */
public interface KbmsFileMainService extends IService<KbmsFileMainEntity> {

    AmisCRUDUtils selectByParams(KbmsFileMainDTO kbmsFileMainDTO, int page, int perPage);

    AmisCRUDUtils selectFullText(KbmsFileMainDTO kbmsFileMainDTO, int page, int perPage);

    void addDownloadNumByPrimaryKey(Long id);

    void deleteByPrimaryKey(Long id);

}

