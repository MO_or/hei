package io.hei.modules.kbms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.hei.common.utils.PageUtils;
import io.hei.modules.kbms.entity.KbmsFileInfoEntity;

import java.util.Map;

/**
 * 
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-17 14:19:37
 */
public interface KbmsFileInfoService extends IService<KbmsFileInfoEntity> {

    int insert(KbmsFileInfoEntity kbmsFileInfoEntity);

    PageUtils queryPage(Map<String, Object> params);
}

