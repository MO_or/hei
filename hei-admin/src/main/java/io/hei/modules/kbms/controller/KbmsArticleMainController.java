package io.hei.modules.kbms.controller;

import cn.hutool.core.util.URLUtil;
import cn.hutool.http.HtmlUtil;
import io.hei.common.utils.AmisCRUDUtils;
import io.hei.common.utils.R;
import io.hei.common.validator.ValidatorUtils;
import io.hei.modules.kbms.entity.KbmsArticleContentEntity;
import io.hei.modules.kbms.entity.KbmsArticleMainDTO;
import io.hei.modules.kbms.entity.KbmsArticleMainEntity;
import io.hei.modules.kbms.entity.KbmsFileMainDTO;
import io.hei.modules.kbms.service.KbmsArticleContentService;
import io.hei.modules.kbms.service.KbmsArticleMainService;
import io.hei.modules.sys.controller.AbstractController;
import io.hei.modules.sys.entity.SysUserEntity;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;



/**
 * 文章主档表
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-30 09:05:50
 */
@RestController
@RequestMapping("kbms/kbmsarticlemain")
public class KbmsArticleMainController extends AbstractController{
    @Autowired
    private KbmsArticleMainService kbmsArticleMainService;

    @Autowired
    private KbmsArticleContentService kbmsArticleContentService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        KbmsArticleMainDTO kbmsArticleMainDTO = new KbmsArticleMainDTO();
        kbmsArticleMainDTO.setTitle((String) params.get("title"));

        /**
         * 接收侧边菜单栏联动请求参数
         */
        if(!"".equals(params.get("classifyId")) && params.get("classifyId")!=null){
            kbmsArticleMainDTO.setClassifyIds((String) params.get("classifyId"));
        }

        /**
         * 接收分页参数
         */
        int page = Integer.parseInt((String) params.get("page"));
        int perPage = Integer.parseInt((String) params.get("perPage"));

        AmisCRUDUtils data = kbmsArticleMainService.selectByParams(
                kbmsArticleMainDTO, page, perPage);

        return R.ok().put("data", data);
    }

    /**
     * 全文搜索
     */
    @RequestMapping("/listByFullText")
    public R listByFullText(@RequestParam Map<String, Object> params) {

        final String full_text = (String) params.get("full_text");
        KbmsArticleMainDTO kbmsArticleMain = new KbmsArticleMainDTO();
        kbmsArticleMain.setFullText(full_text);

        /**
         * 接收分页参数
         */
        int page = Integer.parseInt((String) params.get("page"));
        int perPage = Integer.parseInt((String) params.get("perPage"));

        AmisCRUDUtils data = kbmsArticleMainService.selectFullText(
                kbmsArticleMain, page, perPage);

        return R.ok().put("data", data);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{articleId}")
    public R info(@PathVariable("articleId") Long articleId){
        KbmsArticleMainEntity kbmsArticleMain = kbmsArticleMainService.getById(articleId);

        return R.ok().put("kbmsArticleMain", kbmsArticleMain);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody Map<String, Object> params){

        /**
         * 添加文章主表
         */
        final SysUserEntity user = getUser();
        KbmsArticleMainEntity kbmsArticleMain = new KbmsArticleMainEntity();
        kbmsArticleMain.setUserId(user.getUserId());
        kbmsArticleMain.setCreateTime(new Date());
        kbmsArticleMain.setTitle((String) params.get("title"));
        if(!"".equals(params.get("classify_id")) && params.get("classify_id")!=null){
            kbmsArticleMain.setClassifyId(((Integer) params.get("classify_id")).longValue());
        }
        kbmsArticleMainService.save(kbmsArticleMain);

        /**
         * 添加文章内容表
         */
        KbmsArticleContentEntity kbmsArticleContent = new KbmsArticleContentEntity();
        kbmsArticleContent.setContent((String) params.get("content"));
        kbmsArticleContentService.save(kbmsArticleContent);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody Map<String, Object> params){
        final long article_id = ((Integer) params.get("article_id")).longValue();

        /**
         * 修改文章主表
         */
        KbmsArticleMainEntity kbmsArticleMain = new KbmsArticleMainEntity();
        kbmsArticleMain.setArticleId(article_id);
        kbmsArticleMain.setAlterTime(new Date());
        kbmsArticleMain.setTitle((String) params.get("title"));
        kbmsArticleMain.setClassifyId(((Integer) params.get("classify_id")).longValue());
        kbmsArticleMainService.updateById(kbmsArticleMain);

        /**
         * 修改文章内容表
         */
        KbmsArticleContentEntity kbmsArticleContent = new KbmsArticleContentEntity();
        kbmsArticleContent.setArticleId(article_id);
        kbmsArticleContent.setContent((String) params.get("article_content"));
        kbmsArticleContentService.updateById(kbmsArticleContent);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete/{article_id}")
    public R delete(@PathVariable("article_id") Long article_id){
        KbmsArticleMainEntity kbmsArticleMain = new KbmsArticleMainEntity();
        kbmsArticleMain.setArticleId(article_id);
        kbmsArticleMain.setDeleteStatus(1);
        kbmsArticleMainService.updateById(kbmsArticleMain);

        return R.ok();
    }

    /**
     * 增加浏览量
     */
    @RequestMapping("/addBrowseNumByArticleId/{article_id}")
    public R addBrowseNumByArticleId(@PathVariable("article_id") Long article_id){
        KbmsArticleMainEntity kbmsArticleMainById = kbmsArticleMainService.getById(article_id);
        KbmsArticleMainEntity kbmsArticleMain = new KbmsArticleMainEntity();
        kbmsArticleMain.setBrowseNum(kbmsArticleMainById.getBrowseNum() + 1);
        kbmsArticleMain.setArticleId(article_id);
        kbmsArticleMainService.updateById(kbmsArticleMain);

        return R.ok();
    }


    @PostMapping("/export")
    public void export(@RequestBody Map<String, Object> params, HttpServletResponse response){
        kbmsArticleMainService.export(params, response);
    }

}
