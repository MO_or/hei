package io.hei.modules.kbms.dao;

import io.hei.modules.kbms.entity.KbmsFileTagRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 文档标签关系表
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-31 15:28:24
 */
@Mapper
public interface KbmsFileTagRelationDao extends BaseMapper<KbmsFileTagRelationEntity> {
	
}
