package io.hei.modules.kbms.controller;

import java.util.Arrays;
import java.util.Map;

import io.hei.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.hei.modules.kbms.entity.KbmsFileInfoEntity;
import io.hei.modules.kbms.service.KbmsFileInfoService;
import io.hei.common.utils.PageUtils;
import io.hei.common.utils.R;



/**
 * 
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-17 14:19:37
 */
@RestController
@RequestMapping("sys/kbmsfileinfo")
public class KbmsFileInfoController {
    @Autowired
    private KbmsFileInfoService kbmsFileInfoService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = kbmsFileInfoService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{fileId}")
    public R info(@PathVariable("fileId") Long fileId){
        KbmsFileInfoEntity kbmsFileInfo = kbmsFileInfoService.getById(fileId);

        return R.ok().put("kbmsFileInfo", kbmsFileInfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody KbmsFileInfoEntity kbmsFileInfo){
        kbmsFileInfoService.save(kbmsFileInfo);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody KbmsFileInfoEntity kbmsFileInfo){
        ValidatorUtils.validateEntity(kbmsFileInfo);
        kbmsFileInfoService.updateById(kbmsFileInfo);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] fileIds){
        kbmsFileInfoService.removeByIds(Arrays.asList(fileIds));

        return R.ok();
    }

}
