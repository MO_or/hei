package io.hei.modules.kbms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.hei.common.utils.AmisOptionsUtils;
import io.hei.common.utils.PageUtils2;
import io.hei.modules.kbms.entity.KbmsFileTagEntity;

import java.util.Map;

/**
 * 文件标签表
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-04 14:25:22
 */
public interface KbmsFileTagService extends IService<KbmsFileTagEntity> {

    PageUtils2 queryPage(Map<String, Object> params);

    AmisOptionsUtils selectAllTags();
}

