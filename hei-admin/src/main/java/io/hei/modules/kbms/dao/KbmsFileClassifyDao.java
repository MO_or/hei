package io.hei.modules.kbms.dao;

import io.hei.modules.kbms.entity.KbmsFileClassifyEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-05 14:17:43
 */
@Mapper
@Repository
public interface KbmsFileClassifyDao extends BaseMapper<KbmsFileClassifyEntity> {

    List<KbmsFileClassifyEntity> selectAll();
}
