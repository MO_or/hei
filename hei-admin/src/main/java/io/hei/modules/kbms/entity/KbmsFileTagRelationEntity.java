package io.hei.modules.kbms.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 文档标签关系表
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-31 15:28:24
 */
@Data
@TableName("kbms_file_tag_relation")
public class KbmsFileTagRelationEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键编号
	 */
	@TableId
	private Long id;
	/**
	 * 标签编号
	 */
	private Long tagId;
	/**
	 * 文档编号
	 */
	private Long fileId;

}
