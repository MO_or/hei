package io.hei.modules.kbms.dao;

import io.hei.modules.kbms.entity.KbmsFileMainEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.hei.modules.kbms.entity.KbmsFileMainDTO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 文件主档表
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-06-29 14:24:48
 */
@Mapper
@Repository
public interface KbmsFileMainDao extends BaseMapper<KbmsFileMainEntity> {

    int deleteByPrimaryKey(Long id);

    List<Map<String, ?>> selectByParams(KbmsFileMainDTO kbmsFileMainDTO);

    List<Map<String, ?>> selectFullText(KbmsFileMainDTO kbmsFileMainDTO);

    int addDownloadNumByPrimaryKey(Long id, Long downloadNum);

    Long selectDownLoadNumByPrimaryKey(Long id);

}
