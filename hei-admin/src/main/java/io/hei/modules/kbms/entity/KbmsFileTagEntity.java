package io.hei.modules.kbms.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 文件标签表
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-04 14:25:22
 */
@Data
@TableName("kbms_file_tag")
public class KbmsFileTagEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 标签编号
	 */
	@TableId
	private Long tagId;
	/**
	 * 标签名
	 */
	private String tagName;

}
