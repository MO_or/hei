package io.hei.modules.kbms.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 文章内容表
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-30 09:05:53
 */
@Data
@TableName("kbms_article_content")
public class KbmsArticleContentEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 文章编号
	 */
	@TableId
	private Long articleId;
	/**
	 * 文章内容
	 */
	private String content;

}
