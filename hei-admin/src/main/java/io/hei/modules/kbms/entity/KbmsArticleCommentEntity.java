package io.hei.modules.kbms.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 文章评论表
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-30 09:06:12
 */
@Data
@TableName("kbms_article_comment")
public class KbmsArticleCommentEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 评论编号
	 */
	@TableId
	private Long commentId;
	/**
	 * 文章编号
	 */
	private Long articleId;
	/**
	 * 评论用户编号
	 */
	private Long commentUserId;
	/**
	 * 目标用户编号
	 */
	private Long userId;
	/**
	 * 评论内容
	 */
	private String content;
	/**
	 * 评论时间
	 */
	private Date commentTime;

}
