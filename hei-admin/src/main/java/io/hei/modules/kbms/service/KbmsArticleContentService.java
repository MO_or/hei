package io.hei.modules.kbms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.hei.common.utils.PageUtils;
import io.hei.modules.kbms.entity.KbmsArticleContentEntity;

import java.util.Map;

/**
 * 文章内容表
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-30 09:05:53
 */
public interface KbmsArticleContentService extends IService<KbmsArticleContentEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

