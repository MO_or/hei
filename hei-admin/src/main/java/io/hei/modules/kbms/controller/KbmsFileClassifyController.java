package io.hei.modules.kbms.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.json.JSONUtil;
import io.hei.common.validator.ValidatorUtils;
import io.hei.modules.kbms.entity.KbmsFileClassifyVO;
import io.hei.modules.kbms.service.impl.KbmsFileClassifyServiceImpl;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.hei.modules.kbms.entity.KbmsFileClassifyEntity;
import io.hei.modules.kbms.service.KbmsFileClassifyService;
import io.hei.common.utils.PageUtils;
import io.hei.common.utils.R;



/**
 * 
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-05 14:17:43
 */
@RestController
@RequestMapping("kbms/kbmsfileclassify")
public class KbmsFileClassifyController {
    @Autowired
    private KbmsFileClassifyService kbmsFileClassifyService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:kbmsfileclassify:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = kbmsFileClassifyService.queryPage(params);

        return R.ok().put("page", page);
    }

    @RequestMapping("/selectAll")
    public R selectAll(){

        List<KbmsFileClassifyEntity> list = kbmsFileClassifyService.selectAll();

        TreeNodeConfig treeNodeConfig = new TreeNodeConfig();
        treeNodeConfig.setWeightKey("weight");
        treeNodeConfig.setIdKey("id");
        treeNodeConfig.setDeep(6);

        List<Tree<String>> treeNodes = TreeUtil.build(list, "0", treeNodeConfig,
                (treeNode, tree) -> {
                    tree.setId(treeNode.getId().toString());
                    tree.setParentId(treeNode.getParentId().toString());
                    tree.setWeight(treeNode.getWeight());
                    tree.setName(treeNode.getName());
                    tree.putExtra("value", treeNode.getId());
                    tree.putExtra("label", treeNode.getName());
                });

        return R.ok().put("data", treeNodes);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:kbmsfileclassify:info")
    public R info(@PathVariable("id") Integer id){
        KbmsFileClassifyEntity kbmsFileClassify = kbmsFileClassifyService.getById(id);

        return R.ok().put("kbmsFileClassify", kbmsFileClassify);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody Map<String, Object> params){
        kbmsFileClassifyService.save(params);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody Map<String, Object> params){

        KbmsFileClassifyEntity kbmsFileClassify = new KbmsFileClassifyEntity();
        kbmsFileClassify.setId(Integer.parseInt((String) params.get("id")));
        kbmsFileClassify.setName((String) params.get("label"));

        ValidatorUtils.validateEntity(kbmsFileClassify);
        kbmsFileClassifyService.updateById(kbmsFileClassify);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Integer[] ids){
        kbmsFileClassifyService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }



}
