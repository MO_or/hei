package io.hei.modules.kbms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.hei.common.utils.PageUtils;
import io.hei.modules.kbms.entity.KbmsFileClassifyEntity;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-05 14:17:43
 */
public interface KbmsFileClassifyService extends IService<KbmsFileClassifyEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<KbmsFileClassifyEntity> selectAll();

    void save(Map<String, Object> params);
}

