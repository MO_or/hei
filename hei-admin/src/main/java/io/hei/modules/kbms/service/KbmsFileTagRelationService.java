package io.hei.modules.kbms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.hei.common.utils.PageUtils;
import io.hei.modules.kbms.entity.KbmsFileTagRelationEntity;

import java.util.Map;

/**
 * 文档标签关系表
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-31 15:28:24
 */
public interface KbmsFileTagRelationService extends IService<KbmsFileTagRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

