package io.hei.modules.kbms.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.hei.common.utils.PageUtils;
import io.hei.common.utils.Query;

import io.hei.modules.kbms.dao.KbmsArticleContentDao;
import io.hei.modules.kbms.entity.KbmsArticleContentEntity;
import io.hei.modules.kbms.service.KbmsArticleContentService;


@Service("kbmsArticleContentService")
public class KbmsArticleContentServiceImpl extends ServiceImpl<KbmsArticleContentDao, KbmsArticleContentEntity> implements KbmsArticleContentService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<KbmsArticleContentEntity> page = this.page(
                new Query<KbmsArticleContentEntity>().getPage(params),
                new QueryWrapper<KbmsArticleContentEntity>()
        );

        return new PageUtils(page);
    }

}
