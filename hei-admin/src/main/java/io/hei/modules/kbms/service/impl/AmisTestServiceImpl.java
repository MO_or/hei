package io.hei.modules.kbms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.hei.common.utils.PageUtils2;
import io.hei.common.utils.Query;
import io.hei.modules.kbms.dao.AmisTestDao;
import io.hei.modules.kbms.entity.AmisTestEntity;
import io.hei.modules.kbms.service.AmisTestService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("amisTestService")
public class AmisTestServiceImpl extends ServiceImpl<AmisTestDao, AmisTestEntity> implements AmisTestService {

    @Override
    public PageUtils2 queryPage(Map<String, Object> params) {
        String test1 = (String)params.get("test1");
        String test2 = (String)params.get("test2");

        IPage<AmisTestEntity> page = this.page(
                new Query<AmisTestEntity>().getPage(params),
                new QueryWrapper<AmisTestEntity>()
                        .like(StringUtils.isNotBlank(test1),"test1", test1)
                        .like(StringUtils.isNotBlank(test2),"test2", test2)
        );

        return new PageUtils2(page);
    }

}
