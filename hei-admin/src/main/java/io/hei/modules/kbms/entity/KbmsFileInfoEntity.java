package io.hei.modules.kbms.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-17 14:19:37
 */
@Data
@TableName("kbms_file_info")
public class KbmsFileInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 文件编号
	 */
	@TableId
	private String fileId;
	/**
	 * 存储路径
	 */
	private String path;
	/**
	 * 文件名称
	 */
	private String fileName;
	/**
	 * 文件类型
	 */
	private String fileType;
	/**
	 * 文件大小
	 */
	private Long fileSize;
	/**
	 * 上传用户
	 */
	private Long userId;
	/**
	 * 上传时间
	 */
	private Date uploadTime;

}
