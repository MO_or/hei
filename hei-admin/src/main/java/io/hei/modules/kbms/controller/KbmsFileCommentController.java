package io.hei.modules.kbms.controller;

import io.hei.common.utils.AmisCRUDUtils;
import io.hei.common.utils.R;
import io.hei.common.validator.ValidatorUtils;
import io.hei.modules.kbms.entity.KbmsFileCommentEntity;
import io.hei.modules.kbms.service.KbmsFileCommentService;
import io.hei.modules.sys.controller.AbstractController;
import io.hei.modules.sys.entity.SysUserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;



/**
 * 文件评论表
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-27 10:50:07
 */
@RestController
@RequestMapping("kbms/kbmsfilecomment")
public class KbmsFileCommentController extends AbstractController {
    @Autowired
    private KbmsFileCommentService kbmsFileCommentService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){

        /**
         * 接收分页参数
         */
        int page = Integer.parseInt((String) params.get("page"));
        int perPage = Integer.parseInt((String) params.get("perPage"));
        Long id = Long.parseLong((String) params.get("id"));

        AmisCRUDUtils data = kbmsFileCommentService.selectByFileId(
                id, page, perPage);

        return R.ok().put("data", data);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{commentId}")
    public R info(@PathVariable("commentId") Long commentId){
        KbmsFileCommentEntity kbmsFileComment = kbmsFileCommentService.getById(commentId);

        return R.ok().put("kbmsFileComment", kbmsFileComment);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody Map<String, Object> params) {

        final SysUserEntity user = getUser();
        KbmsFileCommentEntity kbmsFileCommentEntity = new KbmsFileCommentEntity();
        kbmsFileCommentEntity.setFileUserId(user.getUserId());
        kbmsFileCommentEntity.setUserId(((Integer) params.get("user_id")).longValue());
        kbmsFileCommentEntity.setCommentTime(new Date());
        kbmsFileCommentEntity.setContent((String) params.get("comment_content"));
        kbmsFileCommentEntity.setFileId(((Integer) params.get("id")).longValue());

        kbmsFileCommentService.save(kbmsFileCommentEntity);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody KbmsFileCommentEntity kbmsFileComment){
        ValidatorUtils.validateEntity(kbmsFileComment);
        kbmsFileCommentService.updateById(kbmsFileComment);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] commentIds){
        kbmsFileCommentService.removeByIds(Arrays.asList(commentIds));

        return R.ok();
    }

}
