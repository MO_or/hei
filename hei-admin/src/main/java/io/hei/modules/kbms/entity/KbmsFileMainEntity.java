package io.hei.modules.kbms.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 文件主档表
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-06-29 14:24:48
 */
@Data
@TableName("kbms_file_main")
public class KbmsFileMainEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 文档编号
	 */
	@TableId
	private Long id;
	/**
	 * 文件编号
	 */
	private String file_id;
	/**
	 * 用户编号
	 */
	private Long user_id;
	/**
	 * 创建时间
	 */
	private Date create_time;
	/**
	 * 分类编号
	 */
	private Long classify_id;
	/**
	 * 文件名称
	 */
	private String file_name;
	/**
	 * 评论数
	 */
	private Integer comment_num;
	/**
	 * 下载数
	 */
	private Integer download_num;
	/**
	 * 点赞数
	 */
	private Integer like_num;
	/**
	 * 是否删除：0、否，1、是，默认0
	 */
	private Integer delete_status;

}
