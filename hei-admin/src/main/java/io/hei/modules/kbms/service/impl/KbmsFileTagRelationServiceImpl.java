package io.hei.modules.kbms.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.hei.common.utils.PageUtils;
import io.hei.common.utils.Query;

import io.hei.modules.kbms.dao.KbmsFileTagRelationDao;
import io.hei.modules.kbms.entity.KbmsFileTagRelationEntity;
import io.hei.modules.kbms.service.KbmsFileTagRelationService;


@Service("kbmsFileTagRelationService")
public class KbmsFileTagRelationServiceImpl extends ServiceImpl<KbmsFileTagRelationDao, KbmsFileTagRelationEntity> implements KbmsFileTagRelationService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<KbmsFileTagRelationEntity> page = this.page(
                new Query<KbmsFileTagRelationEntity>().getPage(params),
                new QueryWrapper<KbmsFileTagRelationEntity>()
        );

        return new PageUtils(page);
    }

}
