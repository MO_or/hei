package io.hei.modules.kbms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.hei.common.utils.AmisCRUDUtils;
import io.hei.common.utils.PageUtils;
import io.hei.modules.kbms.entity.KbmsArticleCommentEntity;

import java.util.Map;

/**
 * 文章评论表
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-30 09:06:12
 */
public interface KbmsArticleCommentService extends IService<KbmsArticleCommentEntity> {

    PageUtils queryPage(Map<String, Object> params);

    AmisCRUDUtils selectByArticleId(Long articleId, int page, int perPage);
}

