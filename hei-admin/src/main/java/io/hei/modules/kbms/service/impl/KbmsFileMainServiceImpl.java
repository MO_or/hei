package io.hei.modules.kbms.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.hei.common.utils.AmisCRUDUtils;
import io.hei.modules.kbms.dao.KbmsFileTagDao;
import io.hei.modules.kbms.entity.KbmsFileMainDTO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import io.hei.modules.kbms.dao.KbmsFileMainDao;
import io.hei.modules.kbms.entity.KbmsFileMainEntity;
import io.hei.modules.kbms.service.KbmsFileMainService;


@Service("kbmsFileMainService")
public class KbmsFileMainServiceImpl extends ServiceImpl<KbmsFileMainDao, KbmsFileMainEntity> implements KbmsFileMainService {

    @Autowired
    private KbmsFileMainDao kbmsFileMainDao;

    @Autowired
    private KbmsFileTagDao kbmsFileTagDao;

    @Override
    public AmisCRUDUtils selectByParams(KbmsFileMainDTO kbmsFileMainDTO, int page, int perPage) {

        // 查询标签编号对应的文档编号，通过文档编号查询对应文档
        String tagId = kbmsFileMainDTO.getTagId();
        if(!"".equals(tagId) && tagId !=null){
            System.out.println(tagId);
            List<String> file_id = kbmsFileTagDao.selectFileIdByTagId(tagId);
            kbmsFileMainDTO.setIds(StringUtils.join(file_id, ","));
        }

        // 组装分页参数
        PageHelper.startPage(page, perPage);
        List<Map<String, ?>> maps = kbmsFileMainDao.selectByParams(kbmsFileMainDTO);
        PageInfo pageInfo = new PageInfo<>(maps);

        return new AmisCRUDUtils(pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public AmisCRUDUtils selectFullText(KbmsFileMainDTO kbmsFileMainDTO, int page, int perPage) {

        // 组装分页参数
        PageHelper.startPage(page, perPage);
        List<Map<String, ?>> maps = kbmsFileMainDao.selectFullText(kbmsFileMainDTO);
        PageInfo pageInfo = new PageInfo<>(maps);

        return new AmisCRUDUtils(pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public void addDownloadNumByPrimaryKey(Long id) {

        Long downLoadNum = kbmsFileMainDao.selectDownLoadNumByPrimaryKey(id) + 1;
        kbmsFileMainDao.addDownloadNumByPrimaryKey(id, downLoadNum);
    }

    @Override
    public void deleteByPrimaryKey(Long id) {
        kbmsFileMainDao.deleteByPrimaryKey(id);
    }

}
