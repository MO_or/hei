package io.hei.modules.kbms.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-05 14:17:43
 */
@Data
@TableName("kbms_file_classify")
public class KbmsFileClassifyVO extends KbmsFileClassifyEntity implements Serializable {
	/**
	 *
	 */
	@TableId
	private Integer id;
	/**
	 *
	 */
	private String name;
	/**
	 *
	 */
	private Integer parentId;
	/**
	 *
	 */
	private Integer weight;
}
