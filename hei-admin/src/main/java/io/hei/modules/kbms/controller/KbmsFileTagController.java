package io.hei.modules.kbms.controller;

import java.util.Arrays;
import java.util.Map;

import io.hei.common.utils.PageUtils2;
import io.hei.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.hei.modules.kbms.entity.KbmsFileTagEntity;
import io.hei.modules.kbms.service.KbmsFileTagService;
import io.hei.common.utils.PageUtils;
import io.hei.common.utils.R;



/**
 * 文件标签表
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-04 14:25:22
 */
@RestController
@RequestMapping("kbms/kbmsfiletag")
public class KbmsFileTagController {
    @Autowired
    private KbmsFileTagService kbmsFileTagService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils2 data = kbmsFileTagService.queryPage(params);

        return R.ok().put("data", data);
    }

    /**
     * options
     */
    @RequestMapping("/options")
    public R options(){

        return R.ok().put("data", kbmsFileTagService.selectAllTags());
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{tagId}")
    @RequiresPermissions("sys:kbmsfiletag:info")
    public R info(@PathVariable("tagId") Long tagId){
        KbmsFileTagEntity kbmsFileTag = kbmsFileTagService.getById(tagId);

        return R.ok().put("kbmsFileTag", kbmsFileTag);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody Map<String, Object> params){

        KbmsFileTagEntity kbmsFileTag = new KbmsFileTagEntity();
        kbmsFileTag.setTagName((String) params.get("label"));
        kbmsFileTagService.save(kbmsFileTag);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:kbmsfiletag:update")
    public R update(@RequestBody KbmsFileTagEntity kbmsFileTag){
        ValidatorUtils.validateEntity(kbmsFileTag);
        kbmsFileTagService.updateById(kbmsFileTag);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:kbmsfiletag:delete")
    public R delete(@RequestBody Long[] tagIds){
        kbmsFileTagService.removeByIds(Arrays.asList(tagIds));

        return R.ok();
    }

}
