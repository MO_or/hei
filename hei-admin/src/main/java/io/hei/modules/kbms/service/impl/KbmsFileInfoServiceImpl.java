package io.hei.modules.kbms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.hei.common.utils.PageUtils;
import io.hei.common.utils.Query;
import io.hei.modules.kbms.dao.KbmsFileInfoDao;
import io.hei.modules.kbms.entity.KbmsFileInfoEntity;
import io.hei.modules.kbms.service.KbmsFileInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("kbmsFileInfoService")
public class KbmsFileInfoServiceImpl extends ServiceImpl<KbmsFileInfoDao, KbmsFileInfoEntity> implements KbmsFileInfoService {

    @Autowired
    private KbmsFileInfoDao kbmsFileInfoDao;

    @Override
    public int insert(KbmsFileInfoEntity kbmsFileInfoEntity) {
        return kbmsFileInfoDao.insert(kbmsFileInfoEntity);
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<KbmsFileInfoEntity> page = this.page(
                new Query<KbmsFileInfoEntity>().getPage(params),
                new QueryWrapper<KbmsFileInfoEntity>()
        );

        return new PageUtils(page);
    }


}
