package io.hei.modules.kbms.service.impl;

import io.hei.common.utils.AmisOptionsUtils;
import io.hei.common.utils.PageUtils2;
import io.hei.modules.kbms.dao.KbmsFileMainDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.hei.common.utils.PageUtils;
import io.hei.common.utils.Query;

import io.hei.modules.kbms.dao.KbmsFileTagDao;
import io.hei.modules.kbms.entity.KbmsFileTagEntity;
import io.hei.modules.kbms.service.KbmsFileTagService;


@Service("kbmsFileTagService")
public class KbmsFileTagServiceImpl extends ServiceImpl<KbmsFileTagDao, KbmsFileTagEntity> implements KbmsFileTagService {

    @Autowired
    private KbmsFileTagDao kbmsFileTagDao;

    @Override
    public PageUtils2 queryPage(Map<String, Object> params) {
        IPage<KbmsFileTagEntity> page = this.page(
                new Query<KbmsFileTagEntity>().getPage(params),
                new QueryWrapper<KbmsFileTagEntity>()
        );

        return new PageUtils2(page);
    }

    @Override
    public AmisOptionsUtils selectAllTags() {
        return new AmisOptionsUtils(kbmsFileTagDao.selectAllTags());
    }

}
