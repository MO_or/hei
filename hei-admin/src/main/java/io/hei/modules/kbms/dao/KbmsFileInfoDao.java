package io.hei.modules.kbms.dao;

import io.hei.modules.kbms.entity.KbmsFileInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-08-17 14:19:37
 */
@Mapper
@Repository
public interface KbmsFileInfoDao extends BaseMapper<KbmsFileInfoEntity> {
    int insert(KbmsFileInfoEntity record);
}
