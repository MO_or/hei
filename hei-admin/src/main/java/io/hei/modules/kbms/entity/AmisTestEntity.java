package io.hei.modules.kbms.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2021-07-22 15:31:34
 */
@Data
@TableName("amis_test")
public class AmisTestEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private String test1;
	/**
	 * 
	 */
	private String test2;
	/**
	 * 
	 */
	private String test3;
	/**
	 * 
	 */
	private String test4;
	/**
	 * 
	 */
	private String test5;

}
