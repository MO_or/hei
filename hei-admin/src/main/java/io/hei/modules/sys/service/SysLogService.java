

package io.hei.modules.sys.service;


import com.baomidou.mybatisplus.extension.service.IService;
import io.hei.common.utils.PageUtils;
import io.hei.modules.sys.entity.SysLogEntity;

import java.util.Map;


/**
 * 系统日志
 *
 *
 */
public interface SysLogService extends IService<SysLogEntity> {

    PageUtils queryPage(Map<String, Object> params);

}
