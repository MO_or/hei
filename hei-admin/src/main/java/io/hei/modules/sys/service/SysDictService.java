

package io.hei.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.hei.common.utils.PageUtils;
import io.hei.modules.sys.entity.SysDictEntity;

import java.util.Map;

/**
 * 数据字典
 *
 *
 */
public interface SysDictService extends IService<SysDictEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

